resource "aws_vpc" "vpc" {
  cidr_block       = var.vpc_cidr
  # region = var.region
  tags = {
    Name = "${var.project}-${var.env}-vpc"
    Env  = var.env
  }
}

resource "aws_subnet" "pri_subnet" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.pri_subnet_cidr
  availability_zone = var.pri_subnet_az
  tags = {
    Name = "${var.project}-${var.env}-private-subnet"
    Env  = var.env

  }
}

resource "aws_subnet" "pub_subnet" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.pub_subnet_cidr
  map_public_ip_on_launch = true
  availability_zone = var.pub_subnet_az
  tags = {
    Name = "${var.project}-${var.env}-public-subnet"
    Env = var.env
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project}-${var.env}-igw"
    Env = var.env
  }
}

resource "aws_default_route_table" "default_rt" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "${var.project}-${var.env}-default-rt"
    Env = var.env
  }
}

resource "aws_eip" "nat_eip" {
  domain   = "vpc"
  tags = {
    Name = "${var.project}-${var.env}-nat-eip"
    Env = var.env
  }
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.pub_subnet.id

  tags = {
    Name = "${var.project}-${var.env}-nat"
    Env = var.env
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.igw]
}

resource "aws_route_table" "nat_rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat.id
  }

  tags = {
    Name = "${var.project}-${var.env}-nat-rt"
    Env = var.env
  }
}

resource "aws_route_table_association" "pri_subnet_association" {
  subnet_id      = aws_subnet.pri_subnet.id
  route_table_id = aws_route_table.nat_rt.id
}

resource "aws_security_group" "allow_ports" {
  for_each = var.ports
  name        = "{var.project}-${each.key}"
  description = "Allow TLS"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description      = "TLS"
    from_port        = each.value.port
    to_port          = each.value.port
    protocol         = "tcp"
    cidr_blocks      = [each.value.cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.project}-each.key"
  }
}
