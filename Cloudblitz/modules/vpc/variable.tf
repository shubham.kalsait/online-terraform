variable "ports" {
   # type = map(map)
}

variable "project" {}
variable "vpc_cidr" {}
variable "region" {}
variable "env" {}
variable "pri_subnet_cidr" {}
variable "pub_subnet_cidr" {}
variable "pub_subnet_az" {}
variable "pri_subnet_az" {}